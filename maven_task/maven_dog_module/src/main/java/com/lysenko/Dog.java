package com.lysenko;

import com.lysenko.beast.Animal;

public class Dog implements Animal {

    @Override
    public void sound() {
        System.out.println("Gav");
    }
}
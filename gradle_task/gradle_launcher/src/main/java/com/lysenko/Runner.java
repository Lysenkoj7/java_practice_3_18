package com.lysenko;

import com.lysenko.entity.Employee;

public class Runner {
    public static void main(String[] args) {
        Dog dog = new Dog();
        Cat cat = new Cat();
        System.out.println("Realization logic in another modules");

        dog.sound();
        cat.sound();
        System.out.println();
        System.out.println("Using Lombok for generation getters/setters");

        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("Johny");
        employee.setLastName("Rico");
        System.out.println(employee);
    }
}




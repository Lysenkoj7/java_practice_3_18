package com.lysenko.entity;

import lombok.Data;

@Data
public class Employee {

    private int id;
    private String firstName;
    private String lastName;
}



